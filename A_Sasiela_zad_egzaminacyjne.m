% Tre�� zadania:
% Przygotuj symulacj� numeryczn� i animacj� wahad�a matematycznego z uwzgl�dnieniem t�umienia i wymuszenia.
% Pozostaw mo�liwo�� zmiany warunk�w pocz�tkowych oraz parametr�w uk�adu. 

clear

% parametry uk�adu s� zadane jako zmienne- ich warto�� mo�na zmieni� przed symulacj� 

m=0.09;                          % masa punktu materialneg,kg
l=1.5;                           % d�ugo�� wahad�a, m 
g=9.81;                          % warto�� przyspieszenia ziemskiego, m/s^2
k=0.06;                          % wsp�czynnik  potrzebny do t�umienia
f=0;                          % si�a wymuszaj�ca,N
om=3.13;                         % cz�sto�� wymuszaj�ca, rad/s 

% parametry czasowe

t=40 ;                           % czas symulacji, s
dt=0.05;                         % krok czasowy,s
iteracje=t/dt;                   % liczba iteracji
tau = t * sqrt(l/g);             % zast�puje czas
dtau= dt * sqrt(g/l);            % zast�puje dt

% macierze 

teta=zeros(iteracje,1);         % macierz potrzebna do przechowania warto�ci k�ta odchylenia wahad�a
omega=zeros(iteracje,1);        % macierz potrzebna do przechowania warto�cipr�dko�ci k�towej 
alfa=zeros(iteracje,1);         % macierz potrzebna do przechowania warto�ci przyspieszenia k�towego
T=zeros(iteracje,1);            % macierz przechowuj�ca warto�ci czasu zadanego jako tau 

% warunki pocz�tkowe zadane tak�e jako zmienne  

teta(1,1)=pi/4;                 % pocz�tkowe odchylenie
omega(1,1)=0;                   % pocz�tkowa pr�dko��
alfa(1,1)=0;                    % pocz�tkowe przyspieszenie k�towe
T(1,1)=0;                       % na pocz�tku czas jest r�wny 0 s

% otwarcie okienka dla animacji 

animacja=VideoWriter('wahad�o_matematyczne.avi'); % program tworzy plik video na potrzeby animacji 
animacja.FrameRate=1;
open(animacja);

for n=1:iteracje % g��wna p�tla odpowiadaj�ca za symulacj� numeryczn� oraz tworzenie animacji wahad�a
    
    T(n+1,1) = T(n,:) + dtau; % zmiana czasu 
    % rozwi�zanie r�wnania r�niczkowego drugiego stopnia metod� Eulera
    
    teta(n+1,1)=teta(n,:)+(omega(n,:)*dtau);      % obliczanie warto�ci k�ta odchylenia 
    omega(n+1,1)=omega(n,:)+(alfa(n,:)*dtau);     % obliczanie warto�ci cz�sto�ci ko�owej
    alfa(n+1,1)=-sin(teta(n+1,1)) - ((k/m)*sqrt(l/g)*omega(n+1,1)) + (f/(m*g))*cos(om*sqrt(l/g)*T(n+1,1));

    % generowanie wykresu 
    hold on;  % funkcja hold dodaje kolejne punkty z symulacji do tak samo wyskalowanego okna symulacji  
    fill([-l-0.2*l l+0.2*l l+0.2*l -l-0.2*l],[-l-0.2*l -l-0.2*l l+0.2*l l+0.2*l],'w'); % usuwanie poprzednich wykres�w, linijka kodu pochodzi z kodu udost�pnionego na serwisie Youtube.com 
    
    plot([0 -l*sin(teta(n+1,:))],[0 -l*cos(teta(n+1,:))],'m','LineWidth',3); % pokazuje ustawienie nitki wahad�a
    plot(-l*sin(teta(n+1,:)),-l*cos(teta(n+1,:)),'Marker','o','MarkerSize',15,'MarkerFaceColor','m','MarkerEdgeColor','m'); % pokazuje po�o�enie punktu materialnego
    title(' Symulacja  ruchu wahad�a matematycznego') % dodanie tytu�u symulacji 
    frame=getframe(gcf);                              % dodanie ramki animacji 
    writeVideo(animacja,frame);                       % otwarcie ramki animacji jako pliku 
end

figure;               % otwiera nowe okno dla wykresu 
plot(T,teta)  ;       % wykres 1 -zale�no�ci k�ta odchylenia teta od czasu 
ylabel('Teta');
xlabel('tau');
title('Wykres zale�no�ci k�ta odchylenia teta od tau')
grid on               % dodanie podzia�ki wi�kszej
grid minor            % dodanie podzia�ki mniejszej 

figure                % otworzenie kolejnego okna- dla kolejnego wykresu
plot(T,omega);
ylabel('Omega');
xlabel('tau');
title('Wykres zale�no�ci cz�sto�ci ko�owej omega od tau')
grid on
grid minor 

figure                 % otworzenie okna dla 3 wykresu 
plot(T,alfa);
ylabel('Alfa');
xlabel('tau');
title('Wykres zale�no�ci przyspieszenia k�towego alfa od tau')
grid on
grid minor

close(animacja)